﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;
using log4net;
using log4net.Config;

namespace BulkOfferSheetFileConverter
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            // Initialize log4net
            XmlConfigurator.Configure(new FileInfo("log4net.config"));
            log.Debug("log4net configuration loaded.");


            log.Info("Starting BulkOfferSheetFileConverter...");

            string fileName = args[0].Split(',')[0];
            string inputSheetName = args[0].Split(',')[0];
            string environmentFolderName = "BulkOfferSheets";
            if (args[0].Split(',').Length == 1)
            {
                environmentFolderName = "BulkOfferSheets";
            } else
            {
                environmentFolderName = args[0].Split(',')[1]; //should be either BulkOfferSheets or BulkOfferSheetsPRODUCTION
            }
            log.Info($"Input arguments: fileName = {fileName}, environmentFolderName = {environmentFolderName}");

            DirectoryInfo hdDirectoryInWhichToSearch = GetCSVPathByFileName(environmentFolderName);
            FileInfo[] filesInDir = hdDirectoryInWhichToSearch.GetFiles(fileName + "*.*");
            log.Info($"Found {filesInDir.Length} file(s) matching the pattern: {fileName}.*");

            bool fileForTodayExists = false;
            //Find file in list ordered by last write time, should get most recent just in case older month reports not cleared
            foreach (FileInfo foundFile in filesInDir.OrderByDescending(x => x.LastWriteTime).ToList())
            {
                if (foundFile.LastWriteTime > DateTime.Today && foundFile.LastWriteTime < DateTime.Now)
                {
                    fileForTodayExists = true;
                    fileName = foundFile.FullName;
                    log.Info($"{fileName} is Today's file selected by argument [{args[0]}]. It is identified for conversion.");
                    break;
                }
                if (foundFile.LastWriteTime < DateTime.Today)
                {
                    continue;
                }
            }

            log.Info("Starting file conversion process...");

            if (environmentFolderName.ToLower().Replace(" ", "").Contains("bulkcbp")){
                //This Bulk cbp process currently only applies to Capital One's file
                string[] filePathPieces = fileName.Split('.')[0].Split('\\');
                string xls = GetExcelPathByFileName(environmentFolderName, filePathPieces);

                Excel.Application xl = new Excel.Application();
                try
                {
                    if (fileForTodayExists)
                    {

                        xl.SheetsInNewWorkbook = 1;
                        xl.Visible = true;
                        Excel.Workbook wb = (Workbook)xl.Workbooks.Add();

                        //get Number of columns for this worksheet
                        StreamReader sr = new StreamReader(fileName);
                        int ColumnsCount = sr.ReadLine().Split(',').Count();
                        var list = new List<int>();
                        for (int i = 0; i < ColumnsCount; i++)
                        {
                            list.Add(2);
                        }// list.ToArray() will get us an array with the right number of columns for this template


                        //Open Excel Workbook for conversion.
                        //Excel.Workbook wb = xl.Workbooks.Open(fileName);
                        Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets.get_Item(1);
                        Excel.Range used = ws.UsedRange;
                        used.EntireColumn.AutoFit();
                        xl.DisplayAlerts = false;
                        ImportCSV( fileName,
                         (Excel.Worksheet)(wb.Worksheets[1]),
                         (Excel.Range)(((Excel.Worksheet)wb.Worksheets[1]).get_Range("$A$1")),
                         //new int[] { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 }, 
                         list.ToArray(),
                         true);

                        wb.SaveAs(xls, 51);
                        wb.Close();

                        log.Info("BulkSheetFileConverter: " + inputSheetName + " File conversion process completed.");
                    }
                    else
                    {
                        Excel.Workbook wb = xl.Workbooks.Add();
                        xl.DisplayAlerts = false;
                        wb.SaveAs(xls);
                        wb.Close();
                        log.Info("BulkSheetFileConverter: " + inputSheetName + " File conversion process completed.");
                    }

                }
                catch (Exception e)
                {
                    log.Error("An error occurred:", e);
                }
                finally
                {
                    xl.Quit();
                }

                log.Info("BulkSheetFileConverter: " + inputSheetName + " File conversion process completed.");

            } else
            {
                if (filesInDir.Length == 0 || !fileName.Contains(".csv"))
                {

                        fileName = fileName + " Request Sheet - " + DateTime.Today.ToString("MM dd yyyy") + ".csv";
                        log.Info("No File exists for today, creating new, blank file.");

                }

                string csv = fileName; // @"\\fs12\Database\Atlas\Reports\BulkOfferSheets\" + fileName + ".csv";
                string[] filePathPieces = fileName.Split('.')[0].Split('\\');
                string xls = GetExcelPathByFileName(environmentFolderName, filePathPieces);
                Excel.Application xl = new Excel.Application();
                try
                {
                    if (fileForTodayExists)
                    {
                        xl.SheetsInNewWorkbook = 1;
                        xl.Visible = true;
                        Excel.Workbook wb = (Workbook)xl.Workbooks.Add();

                        log.Info("Starting file import from CSV...");

                        //get Number of columns for this worksheet
                        StreamReader sr = new StreamReader(csv);
                        int ColumnsCount = sr.ReadLine().Split(',').Count(); //Array.ConvertAll(sr.ReadLine().Split(','), Double.Parse).Length;
                        var list = new List<int>();
                        for (int i = 0; i < ColumnsCount; i++)
                        {
                            list.Add(2);
                        }// list.ToArray() will get us an array with the right number of columns for this template

                        //Open Excel Workbook for conversion.
                        Excel.Worksheet ws = (Excel.Worksheet)wb.Worksheets.get_Item(1);
                        Excel.Range used = ws.UsedRange;
                        used.EntireColumn.AutoFit();
                        xl.DisplayAlerts = false;

                        log.Info("Importing data from CSV...");

                        ImportCSV(fileName,
                            (Excel.Worksheet)(wb.Worksheets[1]),
                            (Excel.Range)(((Excel.Worksheet)wb.Worksheets[1]).get_Range("$A$1")),
                            list.ToArray(), true);

                        string[] columnNames = File.ReadLines(fileName).First().Split(',');
                        if (fileName.ToLower().Replace(" ", "").Contains("phillipscohen"))
                        {
                            List<int> listOfIndex = GetPhillipsCohenColsNeedUpdated(columnNames);
                            UpdateColumnInNumberFormat((Excel.Worksheet)(wb.Worksheets[1]), listOfIndex);
                        }

                        wb.SaveAs(xls, 51);
                        wb.Close();

                        if (fileName.Contains("Portfolio"))
                        {
                            log.Info("Copying additional file...");
                            File.Copy(fileName.Split('.')[0] + ".txt", @"\\fs12\Database\Atlas\Reports\" + environmentFolderName + @"\ExcelVersions\" + (fileName.Split('\\')[fileName.Split('\\').Length - 1]).Split('.')[0] + ".txt");
                        }

                        log.Info("BulkSheetFileConverter: " + inputSheetName + " File conversion process completed.");
                    }
                    else
                    {
                        Excel.Workbook wb = xl.Workbooks.Add();
                        xl.DisplayAlerts = false;
                        wb.SaveAs(xls);
                        wb.Close();

                        if (fileName.Contains("Portfolio"))
                        {
                            fileName = fileName + " Request Sheet - " + DateTime.Today.ToString("MM dd yyyy") + ".txt";
                            //Console.WriteLine("No File exists for today, so creating new, blank file.");
                            File.WriteAllText(@"\\fs12\Database\Atlas\Reports\" + environmentFolderName + @"\ExcelVersions\" + fileName.Split('\\')[fileName.Split('\\').Length - 1], "");
                        }
                        log.Info("BulkSheetFileConverter "+ inputSheetName + " File conversion process completed.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    log.Error("An error occurred:", e);
                    log.Error("File conversion process failed.");
                }
                finally
                {
                    xl.Quit();
                    log.Info("Excel application closed.");
                }
                
            }
            

            
        }

        /// <summary>
        /// Takes a CSV file and sucks it into the specified worksheet of this workbook at the specified range
        /// </summary>
        /// <param name="importFileName">Specifies the full path to the .CSV file to import</param>
        /// <param name="destinationSheet">Excel.Worksheet object corresponding to the destination worksheet.</param>
        /// <param name="destinationRange">Excel.Range object specifying the destination cell(s)</param>
        /// <param name="columnDataTypes">Column data type specifier array. For the QueryTable.TextFileColumnDataTypes property.</param>
        /// <param name="autoFitColumns">Specifies whether to do an AutoFit on all imported columns.</param>
        public static void ImportCSV(string importFileName, Excel.Worksheet destinationSheet,
            Excel.Range destinationRange, int[] columnDataTypes, bool autoFitColumns)
        {
            try
            {
                log.Info("Importing data from CSV...");

                destinationSheet.QueryTables.Add(
                    "TEXT;" + Path.GetFullPath(importFileName),
                destinationRange, Type.Missing);
                destinationSheet.QueryTables[1].Name = Path.GetFileNameWithoutExtension(importFileName);
                destinationSheet.QueryTables[1].FieldNames = true;
                destinationSheet.QueryTables[1].RowNumbers = false;
                destinationSheet.QueryTables[1].FillAdjacentFormulas = false;
                destinationSheet.QueryTables[1].PreserveFormatting = true;
                destinationSheet.QueryTables[1].RefreshOnFileOpen = false;
                destinationSheet.QueryTables[1].RefreshStyle = XlCellInsertionMode.xlInsertDeleteCells;
                destinationSheet.QueryTables[1].SavePassword = false;
                destinationSheet.QueryTables[1].SaveData = true;
                destinationSheet.QueryTables[1].AdjustColumnWidth = true;
                destinationSheet.QueryTables[1].RefreshPeriod = 0;
                destinationSheet.QueryTables[1].TextFilePromptOnRefresh = false;
                destinationSheet.QueryTables[1].TextFilePlatform = 437;
                destinationSheet.QueryTables[1].TextFileStartRow = 1;
                destinationSheet.QueryTables[1].TextFileParseType = XlTextParsingType.xlDelimited;
                destinationSheet.QueryTables[1].TextFileTextQualifier = XlTextQualifier.xlTextQualifierDoubleQuote;
                destinationSheet.QueryTables[1].TextFileConsecutiveDelimiter = false;
                destinationSheet.QueryTables[1].TextFileTabDelimiter = false;
                destinationSheet.QueryTables[1].TextFileSemicolonDelimiter = false;
                destinationSheet.QueryTables[1].TextFileCommaDelimiter = true;
                destinationSheet.QueryTables[1].TextFileSpaceDelimiter = false;
                destinationSheet.QueryTables[1].TextFileColumnDataTypes = columnDataTypes;

                Console.WriteLine("Importing data...");
                destinationSheet.QueryTables[1].Refresh(false);

                if (autoFitColumns == true)
                {
                    destinationSheet.QueryTables[1].Destination.EntireColumn.AutoFit();
                    log.Info("Columns autofit complete.");
                }
                log.Info("CSV data import completed.");

            }
            catch(Exception ex) 
            {
                log.Error($"Error while importing CSV: {ex.Message}", ex);
            }
        }

        /// <summary>
        /// Get the Phillips Cohen File columns that needs updated
        /// </summary>
        /// <param name="columnNames"></param>
        /// <returns></returns>
        private static List<int> GetPhillipsCohenColsNeedUpdated(String[] columnNames)
        {
            List<int> listOfIndex = new List<int>();
            List<string> PhillipsCohenColumns = new List<string>
            {
                "Total Value of All Payments",
                "Current Balance",
                "Amount_1",
                "Amount_2",
                "Amount_3",
                "Amount_4",
                "Amount_5",
                "Amount_6",
                "Amount_7",
                "Amount_8",
                "Amount_9",
                "Amount_10",
                "Amount_11",
                "Amount_12",
                "Amount_13",
                "Amount_14",
                "Amount_15",
                "Amount_16",
                "Amount_17",
                "Amount_18",
                "Amount_19",
                "Amount_20",
                "Amount_21",
                "Amount_22",
                "Amount_23",
                "Amount_24",
                "Amount_25",
                "Amount_26",
                "Amount_27",
                "Amount_28",
                "Amount_29",
                "Amount_30",
                "Amount_31",
                "Amount_32",
                "Amount_33",
                "Amount_34",
                "Amount_35",
                "Amount_36",
            };

            for (int columnIndex = 0; columnIndex < columnNames.Length; columnIndex++)
            {

                if (PhillipsCohenColumns.Contains(columnNames[columnIndex].Replace("\\", "").Replace("\"", "")))
                {
                    listOfIndex.Add(columnIndex+1);
                }
            }
            return listOfIndex;
        }

        /// <summary>
        /// Updates the column in Number format
        /// </summary>
        /// <param name="worksheet"></param>
        /// <param name="columnIndexes"></param>
        private static void UpdateColumnInNumberFormat(Excel.Worksheet worksheet, List<int> columnIndexes)
        {
            foreach (int columnIndex in columnIndexes)
            {
                Excel.Range columnRange = worksheet.UsedRange.Columns[columnIndex];
                columnRange.NumberFormat = "0.00";
            }
        }


        /// <summary>
        /// Returns the excel file path by enviroment folder name
        /// </summary>
        /// <param name="environmentFolderName"></param>
        /// <param name="filePathPieces"></param>
        /// <returns></returns>
        public static string GetExcelPathByFileName(string environmentFolderName, string [] filePathPieces)
        {
            try
            {
                var filePath = "";
                switch (environmentFolderName.ToLower().Replace(" ", ""))
                {
                    case "velocityinvestments":
                        filePath = @"\\ts01\Creditors\" + environmentFolderName + @"\" + filePathPieces[filePathPieces.Length - 1] + ".xlsx";
                        break;
                    default:
                        filePath = @"\\fs12\Database\Atlas\Reports\" + environmentFolderName + @"\ExcelVersions\" + filePathPieces[filePathPieces.Length - 1] + ".xlsx";
                        break;
                }
                log.Info($"Excel file path determined: {filePath}");
                return filePath;
            }
            catch(Exception ex)
            {
                log.Error($"Error while determining Excel file path: {ex.Message}", ex);
                return string.Empty;
            }
            
        }

        /// <summary>
        /// Returns the excel file path by enviroment folder name
        /// </summary>
        /// <param name="environmentFolderName"></param>
        /// <param name="filePathPieces"></param>
        /// <returns></returns>
        public static DirectoryInfo GetCSVPathByFileName(string environmentFolderName)
        {
            try
            {
                DirectoryInfo filePath = null;
                switch (environmentFolderName.ToLower().Replace(" ", ""))
                {
                    case "velocityinvestments":
                        filePath = new DirectoryInfo(ConfigurationManager.AppSettings["VelocityInvestmentsCSVFilePath"]);
                        break;
                    default:
                        filePath = new DirectoryInfo(@"\\fs12\Database\Atlas\Reports\" + environmentFolderName + @"\");
                        break;
                }
                log.Info($"CSV file path determined: {filePath.FullName}");
                return filePath;
            }
            catch (Exception ex)
            {
                log.Error($"Error while determining CSV file path: {ex.Message}", ex);
                return null;
            }
        }
    }
}
